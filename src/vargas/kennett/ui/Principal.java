package vargas.kennett.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import vargas.kennett.bl.*;

public class Principal {

    //atributos
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Empleado[] listaEmpleados = new Empleado[10];

    public static void main(String[] args) throws IOException {
        menu();

    }

    public static void menu () throws IOException {
        int opcion = 0;
        do{
            System.out.println("***Bienbenido al sistema***");
            System.out.println("1. Registrar Emleado");
            System.out.println("2. Listar vargas.kennett.bl.Empleado");
            System.out.println("3. Salir.");
            System.out.println("Digite una opcion");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while (opcion !=3);
    }

    public static void procesarOpcion (int opcion) throws IOException {
     switch (opcion)
     {
         case 1: registrarEmpleado();
         break;
         case 2: listarEmpleado();
         break;
         case 3:
             System.out.println("Gracias, vuelva pronto");
             System.exit(0);
         default:
             System.out.println("No existe esa opcion");
     }

    }

    public static void registrarEmpleado () throws IOException {
        System.out.print("Ingrese el cedula");
        String cedula = in.readLine();
        System.out.print("Ingrese le nombre");
        String nombre = in.readLine();
        System.out.print("Ingrese el puesto");
        String puesto = in.readLine();

        Empleado empleado =new Empleado(cedula,nombre,puesto);
        for(int x = 0; x < listaEmpleados.length; x++) {
            if (listaEmpleados[x] == null) {
                listaEmpleados[x] = empleado;
                x = listaEmpleados.length;
            }
        }
        listaEmpleados[0] = empleado;
    }
    static public void listarEmpleado () {
        for (int i = 0; i < listaEmpleados.length; i++){
            if (listaEmpleados[i] != null) {
                System.out.println(listaEmpleados[i].toString());
            }
        }
    }
}
